<?php


class SingleObject
{
    private static $instance = null;

    private function SingleObject(){}

    public static function getInstance() :SingleObject{
        if (self::$instance != null){
            return self::$instance;
        }
        self::$instance = new self();
            return self::$instance;
    }
    public function showMessage() :void{
        echo "Hello world!";
    }
}
